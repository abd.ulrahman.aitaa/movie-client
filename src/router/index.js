import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/Auth/LoginView.vue'
import RegisterView from '../views/Auth/RegisterView.vue'

import HomeView from '../views/HomeView.vue'
import MovieDetailView from '../views/MovieDetailView.vue'

import GenreView from '../views/Genres/IndexView.vue'
import GenreCreate from '../views/Genres/CreateView.vue'
import GenreEdit from '../views/Genres/EditView.vue'


import MovieView from '../views/Movies/IndexView.vue'
import MovieCreate from '../views/Movies/CreateView.vue'
import MovieEdit from '../views/Movies/EditView.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },{
      path: '/movies/:id',
      name: 'movieDetails',
      component: MovieDetailView
    },
    {
      path: '/genres',
      name: 'genres',
      component: GenreView
    },
    {
      path: '/genres/create',
      name: 'genreCreate',
      component: GenreCreate
    },
    {
      path: '/genres/:id/edit',
      name: 'genreEdit',
      component: GenreEdit
    },
    {
      path: '/movies',
      name: 'movies',
      component: MovieView
    },
    {
      path: '/movies/create',
      name: 'movieCreate',
      component: MovieCreate
    },
    {
      path: '/movies/:id/edit',
      name: 'movieEdit',
      component: MovieEdit
    },
  ]
})

export default router
